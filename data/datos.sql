﻿--
-- Script was generated by Devart dbForge Studio 2019 for MySQL, Version 8.2.23.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 09/12/2021 13:46:32
-- Server version: 5.7.36-log
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

-- 
-- Dumping data for table categorias
--
INSERT INTO categorias VALUES
(2, 'hombre', 'cazadora'),
(1, 'hombre', 'pantalón'),
(3, 'hombre', 'sudadera'),
(6, 'mujer', 'abrigo'),
(5, 'mujer', 'falda'),
(4, 'mujer', 'pantalón'),
(7, 'niño', NULL);

-- 
-- Dumping data for table prendas
--
INSERT INTO prendas VALUES
(1, 'prenda 1', '1', 180, 'foto1.jpg', 1, 1, NULL, 2),
(2, 'prenda2', '2', 120, 'foto2.jpg', 0, 0, NULL, 2);

-- 
-- Dumping data for table fotos
--
-- Table practica9.fotos does not contain any data (it is empty)

-- 
-- Dumping data for table caracteristicas
--
-- Table practica9.caracteristicas does not contain any data (it is empty)

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;