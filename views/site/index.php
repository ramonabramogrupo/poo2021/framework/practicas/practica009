<?php
    use \yii\widgets\ListView;
    use yii\bootstrap4\Carousel;
?>

<div>
   <?php 
        echo Carousel::widget([
        'items' => $paraCarousel,           
        'options'=>[
            'class'=>'mx-auto col-lg-6 pl-0 pr-0'
            ],
        'controls' => ['<i class="fas fa-arrow-left fa-3x"></i>','<i class="fas fa-arrow-right fa-3x"></i>']
    ]);
    
?>
    
</div>

<div>
   <?php 
        echo Carousel::widget([
        'items' => $elementos,           
        'options'=>[
            'class'=>'mx-auto col-lg-6 pl-0 pr-0'
            ],
        'controls' => ['<i class="fas fa-arrow-left fa-3x"></i>','<i class="fas fa-arrow-right fa-3x"></i>']
    ]);
    
?>
    
</div>

<div>
    <?=    
        ListView::widget([
            "dataProvider" => $dataProvider,
            "itemView" => "_index",
            "itemOptions" => [
            'class' => 'col-lg-4',
            ],
            "options" => [
                'class' => 'row',
            ],
            'layout'=>"{items}"
        ]); 
    ?>

</div>

  
