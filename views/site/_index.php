<?php
use yii\helpers\Html;
?>

<div class="row">
    <div class="col-lg-12">
    <?php
    echo Html::img("@web/imgs/" . $model->id . "/" . $model->foto,[
    "class" =>"card-img-top"
    ]);
    
    ?>
    </div>
    <div class="col-lg-12">
        <div class="bg-warning rounded p-2">Precio:</div>
        <div class="p-1"><?= $model->precio ?> €</div>
        <?= Html::a("Ver mas",["site/ver","id"=>$model->id],["class"=>"btn btn-primary float-right"]) ?>
    </div>
</div>

