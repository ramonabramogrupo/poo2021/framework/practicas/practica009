<?php
    use \yii\widgets\ListView;
    
    $this->params['breadcrumbs'][] = ['label' => $tipo, 'url' => ['index']];
    $this->params['breadcrumbs'][] = $subtipo;
?>

<div>
    <h1><?= $titulo ?></h1>
</div>

<div>
    <?=    
        ListView::widget([
            "dataProvider" => $dataProvider,
            "itemView" => "_categoria",
            "itemOptions" => [
            'class' => 'col-lg-4',
            ],
            "options" => [
                'class' => 'row',
            ],
            'layout'=>"{items}"
        ]); 
    ?>

</div>

  


