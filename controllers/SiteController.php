<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use app\models\Prendas;
use app\models\Categorias;
use app\models\Fotos;
use app\models\Caracteristicas;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\helpers\Html;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        
        /**
         * Consulta para los productos de portada
         */
        
        $consulta=Prendas::find()->where([
            'portada' => 1
        ]);  // select * from prendas where portada=1;
        
        // ejecutar la consulta sin modelo
        // Yii::$app->db->createCommand('select * from prendas where portada=1');
        // preparar la consulta para un gridview o listview
        // $dataProvider=new SqlDataProvider([
        //     "sql" => "select * from prendas where portada=1"
        // ]);
        
        
        $dataProvider=new ActiveDataProvider([
            "query" => $consulta
        ]);
        
        
        /**
         * Mostrar los productos de portada en un carrousel
         */
        
        /**
         * crear un array para el carousel
         */
        
        $paraCarousel=[];
        
        $prendas=$consulta->all();
        foreach($prendas as $prenda){
            
            $imagen=Html::img("@web/imgs/" . $prenda->id . "/" . $prenda->foto,['class'=>'mx-auto col-lg-12 pl-0 pr-0']); // foto de la prenda
            
            $precio='<div class="bg-white text-dark mb-2 p-2">' . $prenda->precio . '</div>' ; // precio prenda
            
            $boton=Html::a("Ver mas",["site/ver","id"=>$prenda->id],["class"=>"btn btn-primary"]); // boton ver mas
            
            $paraCarousel[]=[
                'content' => $imagen,
                'caption' => $precio . $boton,
            ];
        }
        
        
        /*
         * Para mostrar las ofertas en un carrousel 
         */
        
        // hacer una consulta de los productos en oferta
        
        $consulta1=Prendas::find()->where([
            "oferta"=> 1
        ]);
        
        // ejecuto la consulta
        $prendasOferta=$consulta1->all();
        
        // preparar el resultado de la consulta para un carousel
        $elementos=[];
        
        foreach($prendasOferta as $prenda){
            $imagen=Html::img("@web/imgs/" . $prenda->id . "/" . $prenda->foto,['class'=>'mx-auto col-lg-12 pl-0 pr-0']); // foto de la prenda
            
            $precio='<div class="bg-white text-dark mb-2 p-2">' . $prenda->precio . '</div>' ; // precio prenda
            
            $descuento='<div class="bg-white text-dark mb-2 p-2">' . $prenda->descuento . '% </div>' ; // precio prenda
            
            $boton=Html::a("Ver mas",["site/ver","id"=>$prenda->id],["class"=>"btn btn-primary"]); // boton ver mas
                        
            $elementos[]=[
              'content' => $imagen,
              'caption' => $precio . $descuento . $boton
            ];
        }
        
        
        return $this->render('index',[
            "dataProvider" => $dataProvider,
            "paraCarousel" => $paraCarousel,
            "elementos" => $elementos,
        ]);
    }
    
    
    /**
     * esta accion debe mostrar todos los datos del producto seleccionado
     * @param type $id
     */
    public function actionVer($id){
        
    }
    
    
    public function actionCazadoras(){
        
        /**
         * Consulta para los productos que sean cazadoras de hombre
         */
        
        // busco la categoria de cazadoras de hombres
        $categoria= Categorias::find()->where([
            'tipo'=>'hombre',
            'subtipo'=>'cazadora'
        ])->one(); // select * from categorias where tipo="hombre" AND subtipo="cazadora";
            
    
        $consulta=Prendas::find()->where([
            'categoria' => $categoria->id
        ]);  // select * from prendas where categoria=2;
        
        
        $dataProvider=new ActiveDataProvider([
            "query" => $consulta
        ]);
        
        return $this->render("categoria",[
            "dataProvider" => $dataProvider,
            "titulo" => "Cazadoras de hombre",
            'tipo' => "Hombre",
            'subtipo' => "Cazadoras"
        ]);
        
        
    }
    
    public function actionPantalonesh(){
        
        /**
         * Consulta para los productos que sean cazadoras de hombre
         */
        
        // busco la categoria de cazadoras de hombres
        $categoria= Categorias::find()->where([
            'tipo'=>'hombre',
            'subtipo'=>'pantalón'
        ])->one(); // select * from categorias where tipo="hombre" AND subtipo="pantalón";
            
    
        $consulta=Prendas::find()->where([
            'categoria' => $categoria->id
        ]);  // select * from prendas where categoria=2;
        
        
        $dataProvider=new ActiveDataProvider([
            "query" => $consulta
        ]);
        
        return $this->render("categoria",[
            "dataProvider" => $dataProvider,
            "titulo" => "Pantalones de hombre",
        ]);
        
        
    }
    
    public function actionSudaderas(){
        
        /**
         * Consulta para los productos que sean cazadoras de hombre
         */
        
        // busco la categoria de cazadoras de hombres
        $categoria= Categorias::find()->where([
            'tipo'=>'hombre',
            'subtipo'=>'sudadera'
        ])->one(); // select * from categorias where tipo="hombre" AND subtipo="sudadera";
            
    
        $consulta=Prendas::find()->where([
            'categoria' => $categoria->id
        ]);  // select * from prendas where categoria=''
        
        
        $dataProvider=new ActiveDataProvider([
            "query" => $consulta
        ]);
        
        return $this->render("categoria",[
            "dataProvider" => $dataProvider,
            "titulo" => "Pantalones de hombre",
        ]);
        
    } 
    
    public function actionAbrigos(){
        
        /**
         * Consulta para los productos que sean cazadoras de hombre
         */
        
        // busco la categoria de cazadoras de hombres
        $categoria= Categorias::find()->where([
            'tipo'=>'mujer',
            'subtipo'=>'abrigo'
        ])->one(); // select * from categorias where tipo="mujer" AND subtipo="abrigos";
            
    
        $consulta=Prendas::find()->where([
            'categoria' => $categoria->id
        ]);  // select * from prendas where categoria=2;
        
        
        $dataProvider=new ActiveDataProvider([
            "query" => $consulta
        ]);
        
        return $this->render("categoria",[
            "dataProvider" => $dataProvider,
            "titulo" => "Abrigos de Mujer",
        ]);
        
        
    }
    
    public function actionFaldas(){
        
        /**
         * Consulta para los productos que sean cazadoras de hombre
         */
        
        // busco la categoria de cazadoras de hombres
        $categoria= Categorias::find()->where([
            'tipo'=>'mujer',
            'subtipo'=>'falda'
        ])->one(); // select * from categorias where tipo="mujer" AND subtipo="falda";
            
    
        $consulta=Prendas::find()->where([
            'categoria' => $categoria->id
        ]);  // select * from prendas where categoria=''
        
        
        $dataProvider=new ActiveDataProvider([
            "query" => $consulta
        ]);
        
        return $this->render("categoria",[
            "dataProvider" => $dataProvider,
            "titulo" => "Faldas de mujer",
        ]);
        
        
    }
    
    
    public function actionPantalonesm(){
        
        /**
         * Consulta para los productos que sean cazadoras de hombre
         */
        
        // busco la categoria de cazadoras de hombres
        $categoria= Categorias::find()->where([
            'tipo'=>'mujer',
            'subtipo'=>'pantalón'
        ])->one(); // select * from categorias where tipo="mujer" AND subtipo="pantalón";
            
    
        $consulta=Prendas::find()->where([
            'categoria' => $categoria->id
        ]);  // select * from prendas where categoria=2;
        
        
        $dataProvider=new ActiveDataProvider([
            "query" => $consulta
        ]);
        
        return $this->render("categoria",[
            "dataProvider" => $dataProvider,
            "titulo" => "Pantalones de mujer",
        ]);
        
        
    }

  
}
