<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fotos".
 *
 * @property int $id
 * @property string $ruta
 * @property int|null $idprenda
 *
 * @property Prendas $idprenda0
 */
class Fotos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fotos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ruta'], 'required'],
            [['idprenda'], 'integer'],
            [['ruta'], 'string', 'max' => 100],
            [['idprenda'], 'exist', 'skipOnError' => true, 'targetClass' => Prendas::className(), 'targetAttribute' => ['idprenda' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ruta' => 'Ruta',
            'idprenda' => 'Idprenda',
        ];
    }

    /**
     * Gets query for [[Idprenda0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdprenda0()
    {
        return $this->hasOne(Prendas::className(), ['id' => 'idprenda']);
    }
}
